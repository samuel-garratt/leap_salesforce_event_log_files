# frozen_string_literal: true

# An EventLogFile object mapping to a SOQL EventLogFile
class EventLogFile < SoqlData
  # Element for 'Event Type', type 'picklist'
  soql_element :event_type, 'EventType'
  # Element for 'Log File', type 'base64'
  soql_element :log_file, 'LogFile'
end
