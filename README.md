# Retain Salesforce Event log files

This shows a simple ruby script to enable one to download Salesforce's `Event Log Files` using Ruby.  

# Setup
* Install bundler which is capable of installs gems listed in the `Gemfile` 

On command line run `gem install bundler`
* Install gems listed in `Gemfile` with

On command line run `bundle install`
* Run `rake create_oauth` to setup credential files and parameters specific to your environment
* Run `rake download_logs` to download log files. 
(If necessary change the date on line 8 of the `Rakefile` to match the date you want. If it's a historical
date remove the `>` symbol from line 10)

# References

* [Trailhead](https://trailhead.salesforce.com/en/content/learn/modules/event_monitoring/event_monitoring_download)
* [Event log download tutorial](https://www.salesforcehacker.com/2014/11/downloading-event-log-files-using-script.html)

