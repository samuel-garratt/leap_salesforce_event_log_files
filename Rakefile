# frozen_string_literal: true

desc 'Download log'
task :download_logs do
  Rake::Task[:create_oauth].invoke unless File.exist?('config/general.rb')
  require 'leap_salesforce'
  Soaspec::OAuth2.debug_oauth = false # Set to true to debug oauth issues
  DATE = Date.parse 3.days.ago.to_s
  puts "Using date #{DATE}"
  logs = EventLogFile.where(LogDate: ">=#{DATE}")
  dir = File.join 'downloads', DATE.to_s
  mkdir_p dir
  puts 'No logs found!' if logs.size.zero?
  logs.each do |log|
    filename = File.join(dir, "#{log.event_type}.csv")
    puts "Downloading '#{log.event_type}' event to #{filename}"
    file = EventLogFile.new 'Get file', method: :get, suburl: log.log_file
    File.write filename, file.response.body
  end
end

desc 'Create OAuth authentication in files locally'
task :create_oauth do
  require 'leap_salesforce/parameters'
  require 'leap_salesforce/generator/generator'
  require 'leap_salesforce/generator/exe_helpers'
  include LeapSalesforce::ExeHelpers
  include LeapSalesforce::Generator
  @user_key = 'admin'
  LeapSalesforce.salesforce_reachable?
  prod = ENV['use_prod'] || input_for('Using prod? (y/n)')
  LeapSalesforce.environment = prod.downcase == 'y' ? 'prod' : 'test'
  unless LeapSalesforce.environment == 'prod'
    puts 'Update .leap_salesforce.yml to match'
  end
  LeapSalesforce.client_id = ENV['client_id'] || input_for('Client id (Customer Id)')
  LeapSalesforce.client_secret = ENV['client_secret'] || STDIN.getpass('Client secret (Consumer Secret)')
  LeapSalesforce.api_user = ENV['SF_USERNAME'] || input_for('Salesforce username (Input not shown): ')
  LeapSalesforce.password = ENV['password'] || STDIN.getpass('Password (Input not shown): ')
  LeapSalesforce.oauth_working?
  generate_files binding, [{ config: ['general.rb', { credentials: 'salesforce_oauth2.yml' }] }]
end

task default: :download_logs
